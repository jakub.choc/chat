package cmd;

import com.google.inject.AbstractModule;

public class ServerModule extends AbstractModule {

    @Override
    public void configure() {
        bind(IParameterFactory.class).to(ParameterFactory.class);
    }
}

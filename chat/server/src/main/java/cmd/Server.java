package cmd;

import com.google.inject.Inject;

public class Server {

    private final IParameterFactory parameterFactory;

    @Inject
    public Server(IParameterFactory parameterFactory) {
        this.parameterFactory = parameterFactory;
    }

    public void run(String[] args) {
        final IParameterProvider parameters = parameterFactory.getParameters(args);
        System.out.println("Maximum numbers of clients: " + parameters.getInteger(CmdParser.CLIENTS));
    }
}

package org.home;

import cmd.Server;
import cmd.ServerModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class ServerMain {
    public static void main(String[] args) {
        final Injector injector = Guice.createInjector(new ServerModule());
        Server server = injector.getInstance(Server.class);
        server.run(args);
    }
}